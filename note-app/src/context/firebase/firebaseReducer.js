export const ADD_POST = "ADD-POST";
export const DELETE_POST = "DELETE-POST";
export const SHOW_NOTE_LIST = "SHOW-NOTE-LIST";
export const IS_FETCHING = "IS-FETCHING";
export const IS_NULL_LIST = "IS-NULL-LIST";
export const NOTE_SUCCESS = "NOTE-SUCCESS";

export const FirebaseReducer = (state, action) => {
    switch (action.type) {
        case ADD_POST:
            return {...state, notes: [...state.notes, action.payload], isNullList: false};
        case DELETE_POST:
            let notes = state.notes.filter(note => note.id !== action.payload);
            if (notes.length == 0) {
                return {...state, notes: notes, isNullList: true}
            } else {
                return {...state, notes: notes};
            }
        // return {...state, notes: state.notes.filter(note => note.id !== action.payload)};
        case SHOW_NOTE_LIST:
            return {...state, notes: action.payload, isFetching: false};
        case IS_FETCHING:
            return {...state, isFetching: true};
        case IS_NULL_LIST:
            return {...state, isNullList: true};
        case NOTE_SUCCESS:
            return {
                ...state, notes: state.notes.map((note) => {
                    if (note.id === action.id) {
                        note.success = true
                        return note;
                    }
                    return note
                })
            };
        default:
            return state
    }

};
