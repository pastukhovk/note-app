import React, {useContext, useReducer} from "react";
import axios from 'axios'
import {FirebaseContext} from "./firebaseContext";
import {
    ADD_POST,
    DELETE_POST,
    FirebaseReducer,
    IS_FETCHING,
    IS_NULL_LIST,
    NOTE_SUCCESS,
    SHOW_NOTE_LIST
} from "./firebaseReducer";
import {AlertContext} from "../alert/alertContext";



export const FirebaseState = ({children}) => {
    const alert = useContext(AlertContext);
    const url = "https://home-project-3a9bd.firebaseio.com/";
    const initialState = {
        notes:[],
        isFetching: false,
        isNullList: false
    };
    const [state, dispatch] = useReducer(FirebaseReducer, initialState);
//ac
    const Fetching = () => {
        dispatch({
            type: IS_FETCHING
        })
    };
    const showNotes = async () => {
        Fetching();
        const response = await axios.get(`${url}notes.json`);
        if(!response.data) {
            dispatch({type: IS_NULL_LIST})
        }
        const payload = Object.keys(response.data || []).map(key => {
            return {
                ...response.data[key],
                id:key
            }
        });
        dispatch({
            type: SHOW_NOTE_LIST,
            payload
        })

    };

    const addNote = async (title) => {
        const note = {
            title,
            data:new Date().toJSON(),
            success:false
        };
        try {
            const response = await axios.post(`${url}notes.json`,note);

            const payload = {
                ...note,
                id: response.data.name
            };
            console.log("addNote:", response.data);
            dispatch({
                type: ADD_POST,
                payload
            })
        }
        catch (e) {
            throw new Error(e.message);
        }
    };

    const deleteNote = async (id) => {
        await axios.delete(`${url}notes/${id}.json`);
        dispatch({
            type: DELETE_POST,
            payload:id
        });
        alert.show(`Заметка успешно удалена`, "success");

    };
    const changeSuccess = async (id) => {

        await axios.patch(`${url}notes/${id}.json`, {success: true} );
        dispatch({
            type: NOTE_SUCCESS,
            id
        });
    };


    return (
    <FirebaseContext.Provider value={{
        showNotes,addNote,deleteNote,Fetching,changeSuccess,
        isFetching:state.isFetching,
        notes:state.notes,
        isNullList:state.isNullList,
    }}>
        {children}
    </FirebaseContext.Provider>
    )
}