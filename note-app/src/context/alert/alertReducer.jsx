import React from "react";
export const SHOW_ALERT = "SHOW-ALERT";
export const HIDE_ALERT = "HIDE-ALERT";

export const alertReducer = (state, action) => {
    switch (action.type) {
        case SHOW_ALERT :
            return {...state, ...action.payload, visible:true};

        case HIDE_ALERT :
return {...state, visible:false};
        default:
            return state
    }
};