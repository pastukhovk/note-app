import React, {useContext} from "react";
import {AlertContext} from "../context/alert/alertContext";
import {CSSTransition} from "react-transition-group";

export const Alert = () => {
    const props = useContext(AlertContext); // раньше мы бы получили это через пропсы,
    // сейчас через контекст
   // if(!props.alert.visible) {
   //     return null
   // }
    return(
        <CSSTransition
        in={props.alert.visible}
        timeout = {750}
        classNames = {'alert'}
        mountOnEnter
        unmountOnExit
        >
        <div className={`alert alert-${props.alert.type} alert-dismissible`}>
            <strong>Внимание!</strong> {props.alert.text}
            <button onClick={props.hide} type="button" className="close"  aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        </CSSTransition>
    )
};