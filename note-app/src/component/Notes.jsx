import React, {useContext, useState} from "react";
import CSSTransition from "react-transition-group/cjs/CSSTransition";
import TransitionGroup from "react-transition-group/cjs/TransitionGroup";

export const Notes = ({notes, deleteNote, changeSuccess}) => {
    return (
        <TransitionGroup component="ul" className="list-group">
            {notes.map(note => (

                <CSSTransition key={note.id} classNames={'note'} timeout={800}>
                    <li className={'list-group-item note' + (note.success ? " list-group-item-success" : "")}>

                        <div>
                            <strong>{note.title}</strong>
                            <small>{note.data}</small>
                        </div>
                        <div className="btn-group">
                            {!note.success ?
                                <button title="Завершить" type="button mr-3" onClick={() => changeSuccess(note.id)}
                                        className="btn btn-outline-success btn-sm">&#10004;
                                </button> : null}
                            <button title="Удалить" type="button active" onClick={() => deleteNote(note.id)}
                                    className="btn btn-outline-danger btn-sm">&times;
                            </button>

                        </div>
                    </li>
                </CSSTransition>
            ))}
        </TransitionGroup>

    )
};