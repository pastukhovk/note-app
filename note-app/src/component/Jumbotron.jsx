import React from "react";
export const Jumbotron = () => {
    return (
        <div className="jumbotron">
            <div className="container">
                <h1 className="display-4">Список заметок пуст</h1>
                <p className="lead">Вы можете добавить первую заметку с помощью формы, расположенной выше</p>
            </div>
        </div>
    )
};
