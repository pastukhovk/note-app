import React, {useContext, useState} from "react";
import {AlertContext} from "../context/alert/alertContext";
import {FirebaseContext} from "../context/firebase/firebaseContext";

export const Form = () => {
    const [value, setValue] = useState("");
    const props = useContext(AlertContext);
    const fireBase = useContext(FirebaseContext);
    const onChange = (e) => {
        setValue(e.target.value); // контролируемое поле ввода
    };
    const onSubmit = (e) => {
        e.preventDefault();
        if (value.trim() && value.length < 80) {
            fireBase.addNote(value.trim()).then(() => {
                props.show(`Заметка c текстом "${value}" добавлена`, "success");
                setValue("")
            }).catch(() => {
                props.show(`Ошибка`, "danger")
            })

        } else {
            props.show(`Вы пытаетесь добавить пустую или слишком длинную заметку`,)
        }

    }
    return (
        <form onSubmit={onSubmit}>
            <div className="form-group">
                <input type="text" value={value} onChange={onChange} className="form-control"
                       placeholder="Введите текст новой заметки"/>
            </div>
        </form>
    )
};
