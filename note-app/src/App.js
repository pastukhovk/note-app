import React from 'react';
import './App.scss';
import {BrowserRouter, Route} from "react-router-dom";
import Home from "./pages/Home";
import {Navbar} from "./component/Header/Navbar";
import About from "./pages/About";
import {Alert} from "./component/Alert";
import {AlertState} from "./context/alert/AlertState";
import {FirebaseState} from "./context/firebase/firebaseState";


function App() {
    return (
        <BrowserRouter>
                <AlertState>
                    <FirebaseState>
                    <Navbar/>
                    <div className="container mt-3">
                        <Alert/>
                            <Route path='/' exact render={() => <Home/>}/>
                            <Route path='/about' render={() => <About/>}/>
                    </div>
                    </FirebaseState>
                </AlertState>
        </BrowserRouter>
    );
}

export default App;
