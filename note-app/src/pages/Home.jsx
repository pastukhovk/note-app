import React, {useContext, useEffect} from "react";
import {Navbar} from "../component/Header/Navbar";
import {Form} from "../component/Form";
import {Notes} from "../component/Notes";
import {FirebaseContext} from "../context/firebase/firebaseContext";
import {Loader} from "../component/Loader";
import {Jumbotron} from "../component/Jumbotron";

export const Home = () => {
    const {isFetching, notes, showNotes,deleteNote,isNullList,changeSuccess} = useContext(FirebaseContext);
    useEffect(() => {
        showNotes()
    },[]);
    return (
        <React.Fragment>
            <Form/>
            <hr/>
            {isNullList ? <Jumbotron/> :
            isFetching ? <Loader/>:
            <Notes notes={notes} deleteNote = {deleteNote} changeSuccess ={changeSuccess}/> }
        </React.Fragment>
    )

};

export default Home